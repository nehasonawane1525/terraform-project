resource "aws_vpc" "terraform_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "terraform_vpc"
  }
}

resource "aws_subnet" "terraform_private_subnet" {
  vpc_id            = aws_vpc.terraform_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "terraform_private_subnet"
  }
}


resource "aws_ebs_volume" "terraform_volume" {
  availability_zone = "ap-southeast-1a"
  size              = 40
  tags = {
    Name = "terraform_volume"
  }
}

resource "aws_ebs_snapshot" "terraform_snapshot" {
 volume_id =aws_ebs_volume.terraform_volume.id
  tags = {
    Name = "terraform_snapshot"
  }
}

variable "public_key" {
  default = "/home/gitlab-runner/.ssh/id_rsa.pub"
}

resource "aws_key_pair" "terraform_key_pair" {
  key_name   = "terraform_key_pair"
  public_key = "${file(var.public_key)}"
}

resource "aws_instance" "terraform_usecase_instance" {
  ami                 = "ami-097c4e1feeea169e5"  
  instance_type      = "t2.micro"    
  availability_zone = "ap-southeast-1a"
  subnet_id          = aws_subnet.terraform_private_subnet.id
  key_name           = aws_key_pair.terraform_key_pair.id
tags = {
  Name = "terraform_usecase_instance"
}
  }


resource "aws_volume_attachment" "terraform_volume_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.terraform_volume.id
  instance_id = aws_instance.terraform_usecase_instance.id
}
