terraform {
  backend "s3" {
    bucket = "gitlabs3-bucket"
    key    = "global/s3/terraform.tfstate"
    region = "ap-southeast-1"
    dynamodb_table = "terraform-state-table"
  }
}
